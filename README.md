# Thermodynamik mit Jupyter-Notebooks

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/sgeorg%2Fpython-thermo/HEAD)
[![CC BY-SA-3.0](https://img.shields.io/badge/license-CC%20BY--SA%203.0%20DE-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/3.0/de/)
[![Powered by Jupyter](https://img.shields.io/badge/powered%20by-jupyter-orange?logo=python&logoColor=green)](https://jupyter.org/)

Dieses Repository enthält einige [Jupyter Notebooks](https://jupyter.org/), die im Rahmen der Lehrveranstaltung *Thermodynamik* an der [htw saar](https://htwsaar.de) verwendet werden.

## Übersicht

* **Zustandsänderungen.ipynb** (interaktiv)  
Zustandsänderungen des idealen Gases im p-v- und T-s-Diagramm
* **Übung-Zylinder.ipynb** (interaktiv)  
Expansion/Kompression eines idealen Gases in einem geschlossenen System mit Zustands-, Prozessgrößen und Diagrammen
* **Carnotprozess.ipynb**  
Carnotprozess eines idealen Gases mit Zustands-, Prozessgrößen und Diagrammen
* **Clausius-Rankine-Prozess**  
Idealer Prozess der Dampfkraftwerks mit einfacher Zwischenüberhitzung. Mit T-s- und h-s-Diagramm sowie einer tabellatischen Zusammenfassung wichtiger Prozessgrößen und Kennwerte.

## Nutzung

Die Notebooks in diesem Repository können auf zwei Arten verwendet werden:

1. Lokale installation von Jupyter Notebooks auf ihrem Gerät.
    1. Installieren Sie [Python](https://python.org)
    2. Laden Sie das Repository manuell herunter, oder clonen Sie es mit Git:  
`git clone https://gitlab.com/sgeorg/python-thermo`
    3. Installieren Sie die Zusatzpackete:  
`pip install -r requirements.txt`
    4. Starten Sie Jupyter:  
`jupyter notebook`
2. Online-Nutzung über MyBinder.org:  
Gehen Sie auf [https://mybinder.org/v2/gl/sgeorg%2Fpython-thermo/HEAD](https://mybinder.org/v2/gl/sgeorg%2Fpython-thermo/HEAD).

## Copyright

Die Notebooks in diesem Repository werden unter den Bedinungen der *Creative Commons Attribution-ShareAlike 3.0 Deutschland* kostenlos zu Verfügung gestellt.

[CC BY-SA 3.0 DE](https://creativecommons.org/licenses/by-sa/3.0/de/), Sebastian Georg, 2021
